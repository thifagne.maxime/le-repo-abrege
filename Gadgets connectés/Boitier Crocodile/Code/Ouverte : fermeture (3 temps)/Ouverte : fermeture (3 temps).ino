#include <Arduino.h>
#include <Servo.h>

Servo servo;
void twentyMinutes();5
void oneHour();
int menu = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  servo.attach(D7);
  pinMode(D3,INPUT);
  pinMode(D4,INPUT);
  pinMode(D5,INPUT_PULLUP);
  servo.write(0);

}

void loop() {
  // put your main code here, to run repeatedly:
  int bouton1 = digitalRead(D3);
  int bouton2 = digitalRead(D4);
  int bouton3 = digitalRead(D5);
  Serial.print("Bouton 1:");
  Serial.println(bouton1);
  Serial.print("Bouton 2:");
  Serial.println(bouton2);
  Serial.print("Bouton 3:");
  Serial.println(bouton3);
  Serial.println();

  if (bouton3 == 0){
    servo.write(170);
  } else {
    servo.write(7);
  }

  if (bouton3 == 0 && bouton2 == 0){
    oneHour();
  } 

  if (bouton3 == 0 && bouton1 == 0){
    twentyMinutes();
  } 

  delay(1000);

}

void oneHour() {
  servo.write(7);
  delay(3600000);
 } 

void twentyMinutes() {
  servo.write(7);
  delay(1200000);
 } 